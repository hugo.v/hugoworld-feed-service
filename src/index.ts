import 'dotenv/config';
import express from 'express';
import chalk from 'chalk';
import cors from 'cors';

// Services
import database from './services/database/database';
import routes from './services/api/routes';

const log = console.log;

const app = express();

const init = async () => {
    try {
        await database();
        log(chalk.green('Connection to database is successful'));
        app.use(cors());
        app.use(express.json());
        app.listen(process.env.PORT || 5000, () => {
            console.log('Server has started on port ', process.env.PORT || 5000);
        });
        app.use('/api', routes);
    } catch (error) {
        log(error);
    }
};

init();
