const { series, dest, src, watch } = require('gulp');

const babel = require('gulp-babel');
const ts = require('gulp-typescript');
const tsProject = ts.createProject('tsconfig.json');

const typescript = () => tsProject.src().pipe(tsProject()).js.pipe(dest('dist'));

const transpile = () =>
    src('./dist/**/*.js')
        .pipe(
            babel({
                presets: [['@babel/env']],
                plugins: ['@babel/transform-runtime'],
            }),
        )
        .pipe(dest('./dist'));

const assets = () => src('./config.json').pipe(dest('./dist'));

const watchTS = () => watch(['src/**/*.ts'], build)

const build = series(typescript, transpile, assets);

exports.watch = series(build, watchTS);
exports.build = build;
exports.default = build;
