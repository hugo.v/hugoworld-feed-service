export declare type Config = {
    article_output_path: string;
    rt_base_url: string;
};

export declare type ArticleBody = {
    id: string;
    title: string;
    content: string;
    source: string;
    url: string;
};
