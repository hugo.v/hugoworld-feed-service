import 'dotenv/config';
import axios, { AxiosError } from 'axios';
import chalk from 'chalk';

import { scrapRT } from './scraper';
import { ArticleBody } from '../globals';

const log = console.log;

const createArticle = async (article: ArticleBody | null): Promise<void> => {
    try {
        if (article === null) throw new Error();
        log(`http://${process.env.API_URL}/articles`);
        await axios({
            method: 'POST',
            url: `https://${process.env.API_URL}/articles`,
            headers: {
                'Content-Type': 'application/json',
            },
            auth: {
                username: 'admin',
                password: process.env.API_PSW || '',
            },
            data: article,
        });
        log(`Saving article ${chalk.blue(article.title)} - ${chalk.green('[OK]')}`);
    } catch (error: Error | AxiosError | unknown) {
        if (axios.isAxiosError(error)) {
            if (error.response) {
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            }
        }
        if (article !== null) {
            log(chalk.red('Could not create the article: '), article.title);
        } else log(`${chalk.yellow('article doesnt exist, skipping')}`);
    }
};

const main = async () => {
    const rtArticles = await scrapRT();

    const articles = [...rtArticles].filter((article) => article !== null);
    await Promise.all(articles.map((article) => createArticle(article)));
    log(chalk.green.bold('Done !'));
};

main();
