import mongoose from 'mongoose';

const mongoUrl = `mongodb+srv://${process.env.MONGO_USR}:${process.env.MONGO_PSW}@hugoworld-feed.x5ly7.mongodb.net/hugoworld-feed?retryWrites=true&w=majority`;

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
export default (): Promise => mongoose.connect(mongoUrl, { useNewUrlParser: true });
