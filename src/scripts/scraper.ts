import axios from 'axios';
import jsdom from 'jsdom';
import chalk from 'chalk';
import dedent from 'dedent';

import { ArticleBody } from '../globals';
import { getConfig } from '../utils/getConfig.js';

const log = console.log;

const getHeadlineArticlesNames = (homeDom: jsdom.JSDOM) => {
    try {
        const headline1 = homeDom.window.document.querySelector('a.featured__link');
        const [headline2, headline3] = homeDom.window.document.querySelectorAll('a.main__link_column');
        return [headline1, headline2, headline3].map((h) => h?.getAttribute('href'));
    } catch (error) {
        log(error);
    }
};

const getArticlesFromCategory = (homeDom: jsdom.JSDOM, category: string) => {
    try {
        const names = [];
        const franceLink = homeDom.window.document.querySelector(`a.heading__link[href="/${category}"]`);
        const franceArticles = franceLink?.parentElement?.parentElement?.children[1].children;

        if (franceArticles) {
            for (let i = 0; i < franceArticles?.length; i += 1) {
                const newsElement = franceArticles[i];
                names.push(newsElement.querySelector('[href]'));
            }
        }
        return names.map((h) => h?.getAttribute('href'));
    } catch (error) {
        log(error);
    }
};

const getPage = async (url?: string | null | undefined, onFinished?: () => void | undefined): Promise<string> => {
    try {
        const { rt_base_url } = await getConfig();
        const response = await axios({
            method: 'GET',
            url: `${rt_base_url}${url || ''}`,
        });
        if (onFinished) onFinished();
        return response.data;
    } catch (error) {
        log(error);
        return '<div>Error</>';
    }
};

const getArticleFromPage = async (name: string, i: number): Promise<ArticleBody | null> => {
    try {
        const page = await getPage(name, () => {
            log(`Scraping article ${chalk.blue((i + 1).toString())} - ${name} ${chalk.green('[OK]')}`);
        });
        const articleDocument = new jsdom.JSDOM(page).window.document;

        const articleText = articleDocument.querySelector('.article__text')?.firstElementChild;
        articleText?.querySelectorAll('.media').forEach((e) => e.remove());

        const title = articleDocument.querySelector('.article__heading')?.textContent;
        const content = articleText?.textContent?.replace(/\s+/g, ' ').trim();
        const url = `https://francais.rt.com/${name}`;
        const id = `rt-${url.match(/\d+/)}`;

        if (title && content && url) {
            return {
                id,
                title,
                content,
                source: 'rt',
                url,
            };
        }
    } catch (error) {
        log(error);
    }
    return null;
};

const fetchArticlesFromList = async (names: string[]) => {
    try {
        const content = await Promise.all(names.map((name, i) => getArticleFromPage(name, i)));

        return content;
    } catch (error) {
        log(error);
    }
    return [];
};

export const scrapRT = async (): Promise<(ArticleBody | null)[]> => {
    try {
        log(
            chalk.blue(dedent`
                888b. 88888   .d88b.                                
                8  .8   8     YPwww. .d8b 8d8b .d88 88b. .d88b 8d8b 
                8wwK'   8         d8 8    8P   8  8 8  8 8.dP' 8P   
                8  Yb   8      Y88P'  Y8P 8     Y88 88P'  Y88P 8    
                                                    8               
            `),
        );
        const homePage = await getPage();
        const homeDom = new jsdom.JSDOM(homePage);

        const headlines = getHeadlineArticlesNames(homeDom);
        const france = getArticlesFromCategory(homeDom, 'france');

        if (headlines?.length && france?.length) {
            const articles = await fetchArticlesFromList([...headlines, ...france]);
            return articles;
        } else {
            log(chalk.red('There was an error fetching the articles names'));
        }
        log(chalk.bold.green('Done !'));
    } catch (error) {
        //
    }
    return [];
};
