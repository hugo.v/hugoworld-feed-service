import express from 'express';
import basicAuth from 'express-basic-auth';

import Article from '../database/models/Article';

const router = express.Router();
interface ArticlesFilters {
    $text?: {
        $search: string;
    };
    date?: {
        $gte: string;
        $lte: string;
    };
}

const auth = basicAuth({
    users: { admin: process.env.API_PSW || '' },
});

router.get('/articles', async (req, res) => {
    const MAX_ARTICLES = 500;
    const { search, date, last } = req.query;
    const findQueryString: ArticlesFilters = {};
    let articles = [];

    if (search) {
        findQueryString['$text'] = { $search: search.toString() };
    }
    if (date) {
        findQueryString.date = {
            $gte: new Date(`${date}T00:00:00`).toISOString(),
            $lte: new Date(`${date}T23:59:00`).toISOString(),
        };
    }
    if (last) articles = await Article.find(findQueryString).sort({ date: '-1' }).limit(Number(last)).exec();
    else articles = await Article.find(findQueryString).limit(MAX_ARTICLES).exec();
    res.send(articles);
});

router.get('/articles/:id', auth, async (req, res) => {
    try {
        const article = await Article.findOne({ id: req.params.id });
        res.send(article);
    } catch (error) {
        res.status(404);
        res.send({ error: 'Article not found' });
    }
});

router.post('/articles', auth, async (req, res) => {
    try {
        const { title, content, source, url, id } = req.body;

        const article = new Article({
            id,
            title,
            content,
            source,
            url,
            date: new Date(),
        });
        await article.save();
        res.send(article);
    } catch (error) {
        res.status(500);
        res.send({ error: 'Could not create the article' });
    }
});

export default router;
