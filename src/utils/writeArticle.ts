import chalk from 'chalk';
import { mkdir, writeFile } from 'fs/promises';
import { getConfig } from './getConfig.js';

const log = console.log;

export const writeArticle = async (filename: string, content: string): Promise<void> => {
    const { article_output_path } = await getConfig();

    try {
        await mkdir(article_output_path, { recursive: true });
        await writeFile(`${article_output_path}/${filename}`, content);
    } catch (error) {
        switch (error.code) {
            case 'EACCES': {
                log(
                    `${chalk.red(
                        'File writing permission denied when trying to create the article 😒',
                    )} path: ${chalk.blue(article_output_path)}`,
                );
                break;
            }
            default: {
                log(error);
                break;
            }
        }
    }
};
