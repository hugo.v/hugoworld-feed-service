import { readFile } from 'fs/promises';
import { Config } from '../globals';

export const getConfig = async (): Promise<Config> => {
    const config = await readFile('./config.json');
    return JSON.parse(config.toString());
};
